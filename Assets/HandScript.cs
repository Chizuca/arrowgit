﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandScript : MonoBehaviour
{
    public int mSpeed = 5 , rSpeed = 50;
    public KeyCode mAvant, mArriere, mGauche, mDroite, mHaut, mBas;
    public KeyCode rAvant, rArriere, rGauche, rDroite, rHaut, rBas;

    public Collider takeObject , grabobject;
    
    void Update()
    {
        #region Mouvements

        //Mouvement de la main dans tout les axes
        if (Input.GetKey (mAvant))
        {
            transform.Translate(Vector3.forward * (mSpeed * Time.deltaTime));
        }
        if (Input.GetKey (mArriere))
        {
            transform.Translate(-Vector3.forward * (mSpeed * Time.deltaTime));
        }
        if (Input.GetKey (mGauche))
        {
            transform.Translate(Vector3.left * (mSpeed * Time.deltaTime));
        }
        if (Input.GetKey (mDroite))
        {
            transform.Translate(-Vector3.left * (mSpeed * Time.deltaTime));
        }
        if (Input.GetKey (mHaut))
        {
            transform.Translate(Vector3.up * (mSpeed * Time.deltaTime));
        }
        if (Input.GetKey (mBas))
        {
            transform.Translate(-Vector3.up * (mSpeed* Time.deltaTime));
        }

        #endregion

        #region Rotations

        //Rotation de la main dans tout les axes
        if (Input.GetKey (rAvant))
        {
            transform.Rotate(Vector3.forward * (rSpeed * Time.deltaTime));
        }
        if (Input.GetKey (rArriere))
        {
            transform.Rotate(-Vector3.forward * (rSpeed * Time.deltaTime));
        }
        if (Input.GetKey (rGauche))
        {
            transform.Rotate(Vector3.left * (rSpeed * Time.deltaTime));
        }
        if (Input.GetKey (rDroite))
        {
            transform.Rotate(-Vector3.left * (rSpeed * Time.deltaTime));
        }
        if (Input.GetKey (rHaut))
        {
            transform.Rotate(Vector3.up * (rSpeed * Time.deltaTime));
        }
        if (Input.GetKey (rBas))
        {
            transform.Rotate(-Vector3.up * (rSpeed * Time.deltaTime));
        }
        

        #endregion

        #region PickUp

        //On ne peut ramasser la flèche qu'avec la main droite, et seulement si elle peut être prise (cf. pas déjà sur la corde)
        if (Input.GetKeyDown(KeyCode.Mouse0) && takeObject && gameObject.name != "Arc" && FindObjectOfType<Arrow>().canBePicked)
        {
            grabobject = takeObject;
            takeObject.transform.SetParent(gameObject.transform);
        }
        if (Input.GetKeyUp(KeyCode.Mouse0) && grabobject && gameObject.name != "Arc" && FindObjectOfType<Arrow>().canBePicked)
        {
            grabobject.transform.parent = null;
            takeObject = null;
        }

        #endregion
        
    }
    
    private void OnTriggerStay(Collider other)
    {
        takeObject = other;
    }

    private void OnTriggerExit(Collider other)
    {
        takeObject = null;
    }
}
