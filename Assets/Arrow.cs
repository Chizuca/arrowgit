﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public float baseForce;
    public bool canBePicked = true;
    public Rigidbody rb;

    public GameObject bow;

    public float time;

    public GameObject trajPoint;

    public Transform arrowFront;
    public List<GameObject> trajArrowsList;

    public float timer;
    public bool shoot;
    public Transform spawn;

    void Start()
    {
        spawn = GameObject.Find("Spawn").transform;
        rb.useGravity = false;
        bow = GameObject.Find("Arc");
        time = 0f;
    }

    private void Update()
    {
        if (shoot)
        {
             if (timer > 0)
             {
                 timer -=  Time.deltaTime;
             }
             else
             {
                 //Pooling de la flèche, on remet toutes les valeurs à 0 et les paramètres correspondants. On fait ça avec un timer
                 timer = 3f;
                 GameObject arrow = ObjectPooler.Instance.SpawnFromPool("Arrows", spawn);
                 arrow.transform.position = spawn.position;
                 arrow.GetComponent<Rigidbody>().useGravity = false;
                 arrow.GetComponent<Rigidbody>().velocity = Vector3.zero;
                 arrow.GetComponent<Arrow>().canBePicked = true;
                 arrow.GetComponent<Collider>().isTrigger = true;
                 arrow.SetActive(true);
                 foreach (GameObject arrowT in trajArrowsList)
                 {
                     Destroy(arrowT);
                 }
                 trajArrowsList.Clear();
                 gameObject.SetActive(false);
                 shoot = false;
             }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "PivotPointRope")
        {
            canBePicked = false;
            
            //On passe la flèche en enfant du point de pivot de la corde (celui qui sera tiré). On la place aux bonnes coordonnées et à la bonne rotation
            transform.parent = other.transform;
            transform.localPosition = Vector3.zero;
            transform.localEulerAngles = other.transform.forward;
        }
    }

    public void Shot(float pullForce)
    {
        //Le tir de la flèche
        shoot = true;
        GetComponent<Collider>().isTrigger = false;
        rb.AddForce(baseForce * pullForce * bow.transform.forward, ForceMode.Impulse);
        rb.useGravity = true;
        transform.parent = null;
        //transform.rotation = Quaternion.FromToRotation(transform.forward, rb.velocity);

        Vector3 v0 = bow.transform.forward * pullForce * 10f;    //La direction de la flèche avec la force avec laquelle elle sera tiré
        float alpha = -bow.transform.eulerAngles.x;    //L'angle entre le sol et l'arc
        float beta = bow.transform.eulerAngles.y;    //L'angle de rotation de l'arc
        
        
        Debug.Log($"alpha = {alpha} ; beta = {beta}");

        Vector3 omP = v0 * Mathf.Cos(alpha * Mathf.Deg2Rad);    //Le projeté orthogonal de v0 sur le sol

        //Les coordonnées de v0
        float vx0 = omP.magnitude * Mathf.Cos((90 - beta) * Mathf.Deg2Rad);

        float vz0 = omP.magnitude * Mathf.Cos(beta * Mathf.Deg2Rad);

        float vy0 = v0.magnitude * Mathf.Sin(alpha * Mathf.Deg2Rad);
        
        
        
        
        Debug.Log($"vx0 = {vx0}, vz0 = {vz0}, vy0 = {vy0}");
        
        
    //Calcul de coordonnées en fonction du temps (trajectoire)
        for (int i = 0; i < 20; i++)
        {
            //Le système à 3 équations
            float ax = 0;
            float ay = Physics.gravity.y;
            float az = 0;


            float vxT = vx0;
            float vyT = ay * time;
            float vzT = vz0;

            float xT = vxT * time + arrowFront.position.x;
            float yT = .5f * ay * Mathf.Pow(time, 2) + vy0 * time + arrowFront.position.y;
            float zT = vzT * time + arrowFront.position.z;

            
            //L'instantiation des flèches sur la trajectoire
            GameObject trajArrow = Instantiate(trajPoint, new Vector3(xT, yT, zT), Quaternion.identity);
            trajArrowsList.Add(trajArrow);
            
            if(trajArrow.transform.position.y < -4.1f)
                break;
            
            Debug.Log($"Pour time = {time}\n" +
             $"x{time} = {xT}, y{time} = {yT}, z{time} = {zT}");

            time += .2f;
        }

        //Rotation des flèches
        for (int i = 0; i < trajArrowsList.Count; i++)
        {
            if (i + 1 < trajArrowsList.Count)
            {
                trajArrowsList[i].transform.LookAt(trajArrowsList[i + 1].transform);
            }
        }
    }
}
