﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRopes : MonoBehaviour
{
    public GameObject topRope, bottomRope, leftHand;

    public Transform baseBowTransform, maxBowTransform, botPoint, topPoint;
    private Transform saveTopRope, saveBottomRope;

    public float scal;
    
    private bool _canPull;


    // Start is called before the first frame update
    void Start()
    {
        saveBottomRope = bottomRope.transform;
        saveTopRope = topRope.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (_canPull)
        {
            if (Input.GetMouseButton(1))
            {
                //On range 2 vecteurs dans des variables. Le premier est celui qui part du milieu de la corde jusqu'à la main gauche. Le deuxième est celui qui part du milieu de la corde jusqu'au point maximum où l'on peut tirer la corde
                Vector3 bowHand = leftHand.transform.position - baseBowTransform.position;
                Vector3 bowRope = (maxBowTransform.position - baseBowTransform.position).normalized;

                scal = Vector3.Dot(bowHand, bowRope);    //On fait le produit scalaire de ces deux vecteurs. La valeur trouvée nous donnera la distance entre le point de base de ma corde et la flèche.
                
                
               transform.localPosition = new Vector3(0, 0, baseBowTransform.localPosition.z -scal);    //On bouge sur l'axe forward de notre pivot. On retire la valeur du produit scalaire pour la faire bouger en arrière (sur l'axe forward).
                
                //Si la corde est proche du point max ou du point min, on snappe la corde à ce point
               if (Vector3.Distance(transform.position, maxBowTransform.position) <= .1f)
                {
                    transform.localPosition = maxBowTransform.localPosition;
                }
                if (Vector3.Distance(transform.position, baseBowTransform.position) <= .1f)
                {
                    transform.localPosition = baseBowTransform.localPosition;
                }

                //La corde ne peut pas dépasser les points min et max
                if (transform.position.z < maxBowTransform.position.z)
                {
                    transform.localPosition =  new Vector3(transform.localPosition.x, transform.localPosition.x, maxBowTransform.localPosition.z);
                }
                else if (transform.position.z > baseBowTransform.position.z)
                {
                    transform.localPosition =  new Vector3(transform.localPosition.x, transform.localPosition.x, baseBowTransform.localPosition.z);
                }


                //On calcule l'angle de la rotation des cordes. On utilise la formule tan(a) = o / ad, pour avoir a il faut faire a = atan( o / ad).
                //On utilise Atan2 car elle est plus efficace, elle permet de chercher la valeur sur tout le cercle trigonométrique. On la convertit ensuite en degré pour la rotation
                float angleTop = -Mathf.Atan2((topPoint.position - transform.position).magnitude,
                                             (baseBowTransform.position - transform.position).magnitude) * Mathf.Rad2Deg;
        
                topRope.transform.localEulerAngles = new Vector3(0, angleTop, 0);
        
        
                float angleBottom = Mathf.Atan2((botPoint.position - transform.position).magnitude,
                                               (baseBowTransform.position - transform.position).magnitude) * Mathf.Rad2Deg;
        
                bottomRope.transform.localEulerAngles = new Vector3(0, angleBottom, 0);
            }
        }

        
        if (Input.GetMouseButtonUp(1))
        {
            //On tire la flèche et on remet les cordes dans leurs positions initiales
            FindObjectOfType<Arrow>().Shot(scal);
            bottomRope.transform.localRotation = Quaternion.Euler(saveBottomRope.localRotation.x, 90, saveBottomRope.localRotation.z);
            topRope.transform.localRotation = Quaternion.Euler(saveTopRope.localRotation.x, -90, saveTopRope.localRotation.z);;

            transform.position = baseBowTransform.position;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "MainD")
        {
            leftHand = other.gameObject;
            _canPull = true;
        }
    }
}
