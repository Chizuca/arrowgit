﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler Instance;
    public List<GameObject> pooledObjects;
    public List<ObjectPoolItem> itemsToPool;

    public Transform spawnPoint;
    
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } 
        
        else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    //On incrémente la liste avec tous les gameobject qu'on peut possiblement pull, ici, les différentes bullet du joueur
    void Start () {
        pooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in itemsToPool) 
        {
            for (int i = 0; i < item.number; i++) {
                GameObject obj = Instantiate(item.arrow, new Vector3(spawnPoint.position.x + i, spawnPoint.position.y, spawnPoint.position.z), Quaternion.identity);
                pooledObjects.Add(obj);

                if (i != 0)
                    obj.SetActive(false);
            }
        }
    }
    
    //On renvoit un game object qui est pull dans le jeu
    public GameObject SpawnFromPool(string tag, Transform position) {
        
        for (int i = 0; i < pooledObjects.Count; i++) 
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].CompareTag(tag)) 
            {
                return pooledObjects[i];
            }
        }
        
        
        foreach (ObjectPoolItem item in itemsToPool) {
            
            if (item.arrow.CompareTag(tag)) {
                
                if (item.hasToExpand) {
                    GameObject obj = Instantiate(item.arrow);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }
    

    [Serializable] //On crée une classe pour le nombre d'objet à pull, ainsi que l'objet a pull
    public class ObjectPoolItem
    {
        public int number;
        public GameObject arrow;
        public bool hasToExpand;
    }
}
